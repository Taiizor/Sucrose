﻿namespace Sucrose.Shared.Dependency.Enum
{
    internal enum UpdateType
    {
        Empty,
        Update,
        Status,
        Network,
        Updating,
        Releases
    }
}