﻿namespace Sucrose.Memory
{
    public static class Constant
    {
        public const string App = "App";

        public const string Key = "Key";

        public const string Host = "Host";

        public const string Port = "Port";

        public const string Loop = "Loop";

        public const string AApp = "AApp";

        public const string GApp = "GApp";

        public const string UApp = "UApp";

        public const string VApp = "VApp";

        public const string YApp = "YApp";

        public const string WApp = "WApp";

        public const string Adult = "Adult";

        public const string Video = "Video";

        public const string State = "State";

        public const string Start = "Start";

        public const string Delay = "Delay";

        public const string Themes = "Themes";

        public const string Volume = "Volume";

        public const string Refresh = "Refresh";

        public const string Shuffle = "Shuffle";

        public const string Visible = "Visible";

        public const string UserAgent = "UserAgent";

        public const string ThemeType = "ThemeType";

        public const string PlayerType = "PlayerType";

        public const string ScreenType = "ScreenType";

        public const string ScreenIndex = "ScreenIndex";

        public const string StretchType = "StretchType";

        public const string DisplayType = "DisplayType";

        public const string CultureName = "CultureName";

        public const string TitleLength = "TitleLength";

        public const string BackdropType = "BackdropType";

        public const string CefArguments = "CefArguments";

        public const string WebArguments = "WebArguments";

        public const string AdaptiveLayout = "AdaptiveLayout";

        public const string AdaptiveMargin = "AdaptiveMargin";

        public const string StorePagination = "StorePagination";

        public const string LibraryLocation = "LibraryLocation";

        public const string LibrarySelected = "LibrarySelected";

        public const string ExpandScreenType = "ExpandScreenType";

        public const string LibraryPagination = "LibraryPagination";

        public const string DescriptionLength = "DescriptionLength";

        public const string DuplicateScreenType = "DuplicateScreenType";
    }
}